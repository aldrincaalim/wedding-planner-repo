# Wedding Planner

## Project Description
The Wedding Planner is an application for a wedding planner company. Employees to create and manage weddings. It is a micro-services cloud deployed application. The application makes use of many GCP services including, Google Compute Engine, Datastore, Cloud SQL, Datastore, App Engine, Cloud Functions, Cloud Storage and Firebase Hosting.

## Technologies
- Node - version 14.17.6
- PostgreSQL
- React
- JavaScript/TypeScript
- Express
- App Engine
- Cloud Functions
- Cloud Storage
- Firebase
- Datastore

## Features
- Authorization service requiring a password match, generates a object that must be present to render certain aspects
- SQL Database to store wedding information and expenses related to each wedding

## Link to Wedding API
https://gitlab.com/aldrincaalim/wedding-planner-api
