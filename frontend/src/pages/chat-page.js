import React from 'react';
import Chat from "../components/chat";
import Navbar from "../components/navbar";

export default function ChatPage() {

    return(
        <div>
            <Navbar></Navbar>
            <Chat></Chat>
        </div>
    )
}