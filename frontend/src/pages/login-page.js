import React from 'react';
import Login from "../components/login";
import Navbar from "../components/navbar";

export default function LoginPage() {
    return (
        <div>
            <Navbar></Navbar>
            <Login></Login>
        </div>
    )
}