import Navbar from "../components/navbar";
import RegisterWedding from "../components/register-wedding";
import "../styles/generic.css";
import "../styles/navbar.css";

export default function PlannerPage() {

    return (
        <div>
            <Navbar></Navbar>
            <RegisterWedding></RegisterWedding>
        </div>
    )
}