import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import ChatPage from './pages/chat-page';
import LoginPage from './pages/login-page';
import PlannerPage from './pages/planner-page';


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route path="/planner-page">
         <PlannerPage></PlannerPage>
        </Route>
        <Route path="/login-page">
         <LoginPage></LoginPage>
        </Route>
        <Route path="/chat-page">
         <ChatPage></ChatPage>
        </Route>

      </Switch>

    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);


