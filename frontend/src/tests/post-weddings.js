import axios from 'axios';
import { useRef } from 'react';

export default function PostWeddings() {

    const weddingDateInput = useRef();

    // const [wedding, setWedding] = useState();

    // async function postWedding() {
    //     const theWeddingId = Number(weddingIdInput.current.value);
    // }\
    async function postWedding() {
        const sentItem = await axios({
            method: "post",
            url: "weddings",
            data: {
                weddingId: 0,
                weddingDate: weddingDateInput
            }
        })
        console.log(sentItem);
    }
    return (
        <div>
            <h1>Post Wedding</h1>
            <input placeholder="Wedding Date" ref={weddingDateInput}></input>
            <button onClick={postWedding}>Post Wedding</button>
            <p>Hi</p>
        </div>
    )
}