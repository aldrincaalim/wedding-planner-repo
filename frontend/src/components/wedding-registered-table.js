import React from 'react';
import "../styles/table.css";

export default function WeddingRegisteredTable({date, location, name, budget}) {

    return (
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Names</th>
                    <th>Budget</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{date}</td>
                    <td>{location}</td>
                    <td>{name}</td>
                    <td>${budget}</td>
                </tr>
            </tbody>
        </table>
    )
}