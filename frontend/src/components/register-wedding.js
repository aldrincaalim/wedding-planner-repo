import React, { useEffect, useRef, useState } from 'react'
import axios from 'axios'
import WeddingRegisteredTable from './wedding-registered-table';
import "../styles/generic.css";

export default function RegisterWedding() {
    const [weddingSignUp, setWeddingSignUp] = useState(
        { weddingDate: '', weddingLocation: '', weddingName: '', weddingBudget: 0}
    );

    const handleChange = (event) => {
        setWeddingSignUp({...weddingSignUp, [event.target.name]: event.target.value})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        axios.post('http://localhost:5000/weddings', weddingSignUp)
          .then(function (response) {
              console.log(response)
          })
          .catch(function (error) {
              console.log(error)
          }) 
        }

        // SEARCH AND DISPLAY SPECIFIC WEDDING
        // const weddingIdInput = useRef();

        // const [wedding, setWedding] = useState();

        // async function getThatWedding(e) {
        //     const theWedding = Number(weddingIdInput.current.value);
        //     const response = await axios.get(`http://localhost:5000/weddings/${theWedding}`);
        //     const wedding = response.data;
        //     console.log(`From getThatWedding():`, wedding);
        //     setWedding(wedding);
        // } 
        
        // DISPLAY ALL WEDDINGS
        const [weddings, setWeddings] = useState([]);
        
        const fetchWeddings = async() => {
            const {data} = await axios.get(
                "http://localhost:5000/weddings/"
                );
                const weddings = data;
                setWeddings(weddings);
                console.log(weddings);
            };
            
            useEffect(() => {
                fetchWeddings();
            }, []);
            
            // async function getAllWeddings(e) {
                //     const response = await axios.get(`http://localhost:5000/weddings/`);
                //     const weddings = response.data;
                //     console.log('From getAllWeddings():', weddings);
                //     for (const wedding of weddings) {
                    //         console.log(wedding);
                    //         setWedding(wedding);
                    //     }
                    // }
                    
                    // DELETE WEDDING BY ID
                    const weddingIdInput = useRef();
            
                    // const [wedding, setWedding] = useState();
            
                    async function deleteWeddingById(e) {
                        const theWedding = Number(weddingIdInput.current.value);
                        const response = await axios.delete(`http://localhost:5000/weddings/${theWedding}`).then(() => console.log(`Wedding with id ${theWedding} successfully deleted`))
                    } 

    return (
            <div className="container">
                <form className='white' onSubmit={handleSubmit}>
                    <h1 className="grey-text.text-darken-3">Register Wedding</h1>                        
                    <div className="input-field">
                        <label htmlFor="weddingDate">Wedding Date</label>
                        <input type="text" name="weddingDate" value={weddingSignUp.weddingDate} onChange={handleChange} required />
                    </div>
                    <div className="input-field">
                        <label htmlFor="weddingLocation">Wedding Location</label>
                        <input type="text" name="weddingLocation" value={weddingSignUp.weddingLocation} onChange={handleChange} required />
                    </div>
                    <div className="input-field">
                        <label htmlFor="weddingName">Wedding Names</label>
                        <input type="text" name="weddingName" value={weddingSignUp.weddingName} onChange={handleChange} required />
                    </div>
                    <div className="input-field">
                        <label htmlFor="weddingBudget">Wedding Budget</label>
                        <input type="number" name="weddingBudget" value={weddingSignUp.weddingBudget} onChange={handleChange} required />
                    </div>
                    <div className="input-field"> 
                        <button className="btn blue darken-3" type="submit">Sign Up</button>
                    </div>
                </form>

                <h2>Delete Wedding By ID</h2>
                <input type="number" placeholder="Wedding ID" ref={weddingIdInput}></input>
                <button onClick={deleteWeddingById}>Delete That Wedding</button>

                <h2>Wedding Lookup</h2>
                {/* <input type="number" placeholder="Wedding ID" ref={weddingIdInput}></input>
                <button onClick={getThatWedding}>Get That Wedding</button> */}
                {/* {wedding === undefined ? <h3>No Weddings to display</h3> : <WeddingRegisteredTable date={wedding.weddingDate || "No Date"} location={wedding.weddingLocation || "No Location"} name={wedding.weddingName || "No Names"} budget={wedding.weddingBudget || 0}></WeddingRegisteredTable>} */}
                {/* <button onClick={getAllWeddings}>Get All Weddings</button> */}
                <p className="propertiesTable">
                    <span>ID</span>
                    <span>Date</span>
                    <span>Location</span>
                    <span>Names</span>
                    <span>Budget</span>
                </p>

                {weddings.map((wedding) => (
        <p className="displayProperties" key={wedding.weddingId}>
        <span className="displayedWeddingProperties">{wedding.weddingId}</span> 
        <span className="displayedWeddingProperties">{wedding.weddingDate}</span> 
        <span className="displayWeddingProperties">{wedding.weddingLocation}</span>
        <span className="displayWeddingProperties">{wedding.weddingName}</span>
        <span className="displayWeddingProperties">${wedding.weddingBudget}</span>
        </p>
      ))}
            </div>
        );
    }

