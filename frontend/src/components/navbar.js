export default function Navbar() {

    return(
        <nav id="navbar" className="navbar">
            <a href="/login-page">Login</a>
            <a href="/planner-page">Planner Page</a>
            <a href="/chat-page">Chat</a>
        </nav>
    )
}